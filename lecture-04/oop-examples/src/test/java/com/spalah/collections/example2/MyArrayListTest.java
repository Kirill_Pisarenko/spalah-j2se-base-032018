package com.spalah.collections.example2;

import org.junit.Assert;
import org.junit.Test;

public class MyArrayListTest {

    @Test
    public void addTest_1_Elment() {
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);

        int expected = 1;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clearTest() {
        //given
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);
        myCollection.add(2000);
        myCollection.add(3000);

        Assert.assertEquals(3, myCollection.size());
        //when
        int expected = 0;
        myCollection.clear();
        int actual = myCollection.size();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeTest_EMPTY_COLLECTION() {
        MyCollection myCollection = new MyArrayList();

        int expected = 0;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeTest_1_Elment() {
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);

        int expected = 1;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }


    @Test
    public void sizeTest_6_Elment() {
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);

        int expected = 6;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void isEmptyTest_EMPTY_COLLECTION() {
        MyCollection myCollection = new MyArrayList();

        Assert.assertTrue(myCollection.isEmpty());
    }

    @Test
    public void isEmptyTest_1_Elment() {
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);

        Assert.assertFalse(myCollection.isEmpty());
    }

    @Test
    public void isEmptyTest_6_Elment() {
        MyCollection myCollection = new MyArrayList();
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);

        Assert.assertFalse(myCollection.isEmpty());
    }
}