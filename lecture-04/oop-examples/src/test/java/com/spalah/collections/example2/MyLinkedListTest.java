package com.spalah.collections.example2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class MyLinkedListTest {

    @Test
    public void addTest_1_Elment() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);

        int expected = 1;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clearTest() {
        //given
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);
        myCollection.add(2000);
        myCollection.add(3000);

        Assert.assertEquals(3, myCollection.size());
        //when
        int expected = 0;
        myCollection.clear();
        int actual = myCollection.size();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeTest_EMPTY_COLLECTION() {
        MyCollection myCollection = new MyLinkedList();

        int expected = 0;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeTest_1_Elment() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);

        int expected = 1;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }


    @Test
    public void sizeTest_6_Elment() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);

        int expected = 6;
        int actual = myCollection.size();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void isEmptyTest_EMPTY_COLLECTION() {
        MyCollection myCollection = new MyLinkedList();

        Assert.assertTrue(myCollection.isEmpty());
    }

    @Test
    public void isEmptyTest_1_Elment() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);

        Assert.assertFalse(myCollection.isEmpty());
    }

    @Test
    public void isEmptyTest_6_Elment() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);
        myCollection.add(1000);

        Assert.assertFalse(myCollection.isEmpty());
    }

    @Test
    public void iteratorNotNullTest() {
        MyCollection myCollection = new MyLinkedList();
        Iterator<Integer> iterator = myCollection.iterator();
        Assert.assertNotNull(iterator);
    }

    @Test
    public void iteratorEmptyCollectionTest() {
        MyCollection myCollection = new MyLinkedList();
        Iterator<Integer> iterator = myCollection.iterator();
        Assert.assertNotNull(iterator);
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void iteratorTest_1_Element() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);
        Iterator<Integer> iterator = myCollection.iterator();
        Assert.assertNotNull(iterator);
        Assert.assertTrue(iterator.hasNext());
        Integer actualElement = 1000;
        Assert.assertEquals(actualElement, iterator.next());
        Assert.assertFalse(iterator.hasNext());
    }

    //todo rewrite test due to order issue
    @Test
    public void iteratorTest_2_Element() {
        MyCollection myCollection = new MyLinkedList();
        myCollection.add(1000);
        myCollection.add(2000);
        Iterator<Integer> iterator = myCollection.iterator();
        Assert.assertNotNull(iterator);
        Assert.assertTrue(iterator.hasNext());

        Integer actualElement = 1000;
        Assert.assertEquals(actualElement, iterator.next());
        Assert.assertTrue(iterator.hasNext());

        actualElement = 2000;
        Assert.assertEquals(actualElement, iterator.next());
        Assert.assertFalse(iterator.hasNext());
    }
}