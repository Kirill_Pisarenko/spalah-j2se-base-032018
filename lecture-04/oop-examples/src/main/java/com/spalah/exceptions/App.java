package com.spalah.exceptions;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println(todo());
        } catch (Exception e) {
            System.out.println(e.getSuppressed().length);
        }
    }

    public static int todo() throws Exception {
        try {
            throw new Exception("1");
        } finally {
            throw new Exception("2");
        }
    }
}
