package com.spalah.oop.example11;

public class BubleSortDesc extends AbstractBubleSort {
    @Override
    protected int compare(int left, int right) {
        return -Integer.compare(left, right);
    }
}
