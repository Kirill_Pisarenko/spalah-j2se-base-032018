package com.spalah.oop.example11;

public class BlackBoard {

    private int[] arr;
    private int index;
    private int length;

    private final Sort sort;

    public BlackBoard(Sort sort, int maxLength) {
        this.sort = sort;
        this.index = 0;
        this.length = maxLength;
        arr = new int[maxLength];
    }

    public void add(int value) {
        if (index < length) {
            arr[index] = value;
            index++;
        } else {
            System.err.printf("IndexOutOfBound: index=%s, length=%s%n", index, length);
            System.exit(1);
        }
    }

    public void clear() {
        index = 0;
    }

    public void draw() {
        sort.sort(arr, index);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ ");
        for (int i = 0; i < index; i++) {
            stringBuilder.append(arr[i]);
            stringBuilder.append("; ");
        }
        stringBuilder.append("]");
        System.out.println(stringBuilder);
    }
}
