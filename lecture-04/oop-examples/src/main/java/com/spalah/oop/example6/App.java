package com.spalah.oop.example6;

class Point {
    private int x;
    private int y;
    private int z;

    public Point() {
        this(0);
    }

    public Point(int x) {
        this(x, x, x);
    }

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}

public class App {
    public static void main(String[] args) {
        Point point1 = new Point(1, 1, 1);
        Point point2 = new Point(6);
        Point point3 = new Point();

        System.out.println(point1);
        System.out.println(point2);
        System.out.println(point3);
    }
}
