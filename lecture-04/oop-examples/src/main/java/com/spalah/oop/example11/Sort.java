package com.spalah.oop.example11;

public interface Sort {
    void sort(int[] arr, int indexTo);
}
