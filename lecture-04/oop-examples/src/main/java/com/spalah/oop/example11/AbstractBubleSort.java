package com.spalah.oop.example11;

public abstract class AbstractBubleSort implements Sort {

    @Override
    public void sort(int[] array, int indexTo) {
        for (int i = 0; i < indexTo; i++) {
            for (int j = 1; j < indexTo; j++) {
                if (compare(array[j], array[j + 1]) < 0) {
                    int tmp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    protected abstract int compare(int left, int right);
}
