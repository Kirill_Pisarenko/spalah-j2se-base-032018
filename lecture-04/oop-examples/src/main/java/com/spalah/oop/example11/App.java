package com.spalah.oop.example11;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        Sort bubleSortAsc = new BubleSortAsc();
        Sort bubleSortDesc = new BubleSortDesc();
        Sort dummySort = new DummySort();

        sort(bubleSortAsc);
        sort(bubleSortDesc);
        sort(dummySort);
    }

    private static void sort(Sort sort) {
        if (sort instanceof BubleSortAsc) {
            System.out.println("call BubleSortAsc");
        } else if (sort instanceof BubleSortDesc) {
            System.out.println("call BubleSortDesc");
        } else {
            System.out.println("call " + sort.getClass().getName());
        }

        int maxLength = 5;
        BlackBoard blackBoard = new BlackBoard(sort, maxLength);
        blackBoard.add(200);
        blackBoard.add(100);
        blackBoard.add(300);
        blackBoard.draw();
    }
}
