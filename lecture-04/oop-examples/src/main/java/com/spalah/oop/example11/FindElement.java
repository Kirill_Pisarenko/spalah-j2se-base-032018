package com.spalah.oop.example11;

public interface FindElement {
    int find(int[] arr, int index);
}
