package com.bubble.oop.example2;

class Animal {

    public static final String MY_CONST = "CAT";

    private static int count;

    private int id;

    public Animal() {
        this.id = ++count;
    }

    public static void sayHello() {
        System.out.println("Hello");
    }

    public int getId() {
        return id;
    }

    public static int getCount() {
        return count;
    }
}

//static key word example
public class App {
    public static void main(String[] args) {
        System.out.println(Animal.getCount());
        Animal animal1 = new Animal();
        Animal animal2 = new Animal();
        Animal animal3 = new Animal();
        System.out.println(Animal.getCount());
        System.out.println("---------------");

        System.out.println(animal1.getId());
        System.out.println(animal2.getId());
        System.out.println(animal3.getId());
    }
}
