package com.bubble.oop.example3;

class Board {
    private static String message;

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String message) {
        Board.message = message;
    }
}

class Client {
    void writeMsg(String msg) {
        Board.setMessage(msg);
    }
}

public class App {
    public static void main(String[] args) {
        System.out.println("Board: " + Board.getMessage());

        Client client1 = new Client();
        client1.writeMsg("Hello Client1w");
        System.out.println("Board: " + Board.getMessage());

        Client client2 = new Client();
        client2.writeMsg("Hello Client2");
        System.out.println("Board: " + Board.getMessage());
    }
}
