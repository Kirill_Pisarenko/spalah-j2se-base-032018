package com.spalah.oop.example1;

class Animal {

    private static int count;

    private boolean alive;

    public Animal() {
        count++;
    }

    public void whoami() {
        System.out.println("I am Animal");
    }

    public final boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {

        this.alive = alive;
    }

    public static void sayHello(){
        System.out.println("Hello");
    }
}

public class App {
    public static void main(String[] args) {
        Animal.sayHello();
    }
}
