package com.spalah.oop.example8;

class Transport {

    protected String number;

    public Transport(String number) {
        this.number = number;
    }

    void move() {
        System.out.println("Transport move " + number);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

class Car extends Transport {
    public Car(String number) {
        super(number);
    }

    @Override
    void move() {
        System.out.println("Car move " + number);
    }
}

class Plane extends Transport {
    private final int wingLength;

    Plane(String number, int wingLength) {
        super(number);
        this.wingLength = wingLength;
    }

    public int getWingLength() {
        return wingLength;
    }
}

public class App {
    public static void main(String[] args) {
//        Transport transport = new Transport("0000");
//        transport.move();
//        Plane plane = new Plane("0002", 100);
//        plane.move();

        Car car = new Car("0001");
        car.move();
    }
}
