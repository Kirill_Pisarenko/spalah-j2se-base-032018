package com.spalah.oop.example7;

class Transport {
    void move() {
        System.out.println("Transport move");
    }
}

class Car extends Transport {
    private String number;

    public Car(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

class Plane extends Transport {
    private final int wingLength;

    Plane(int wingLength) {
        this.wingLength = wingLength;
    }

    public int getWingLength() {
        return wingLength;
    }
}

public class App {
    public static void main(String[] args) {
        Transport transport = new Transport();
        transport.move();
        Car car = new Car("0001");
        car.move();
        Plane plane = new Plane(100);
        plane.move();
    }
}
