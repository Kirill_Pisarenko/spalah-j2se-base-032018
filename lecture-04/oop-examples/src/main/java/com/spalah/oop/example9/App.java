package com.spalah.oop.example9;

import java.util.ArrayList;
import java.util.List;

class Shape {
    public void draw() {
        System.out.println("Shape");
    }
}

class Rectangle extends Shape {
    @Override
    public void draw() {
        System.out.println("Rectangle");
    }
}
class Triangle extends Shape {
    @Override
    public void draw() {
        System.out.println("Triangle");
    }
}

class Blackboard {

    private List<Shape> shapes = new ArrayList<>();

    public void addShape(Shape shape){
        shapes.add(shape);
    }

    public void clear(){
        shapes.clear();
    }

    public void draw(){
        System.out.println("Blackboard draw <<<");
        for (Shape shape : shapes) {
            shape.draw();
        }
        System.out.println("Blackboard draw >>>");
    }

    public void drawShape(Shape shape){
        shape.draw();
    }
}


public class App {
    public static void main(String[] args) {
        Blackboard blackboard = new Blackboard();

        blackboard.addShape(new Rectangle());
        blackboard.addShape(new Triangle());
        blackboard.addShape(new Shape());

        blackboard.draw();
    }
}
