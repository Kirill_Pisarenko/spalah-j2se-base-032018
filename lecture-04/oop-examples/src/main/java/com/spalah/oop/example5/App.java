package com.spalah.oop.example5;

class Dog {
    private final String name;

    Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}

public class App {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Sashko");
        System.out.println(dog1.getName());
        Dog dog2 = new Dog("Taras");
        System.out.println(dog2.getName());
    }
}
