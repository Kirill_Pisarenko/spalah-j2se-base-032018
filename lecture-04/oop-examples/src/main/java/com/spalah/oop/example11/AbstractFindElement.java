package com.spalah.oop.example11;

public abstract class AbstractFindElement implements FindElement{

    @Override
    public int find(int[] arr, int length) {
        int result = arr[0];
        for (int i = 0; i < length; i++) {
            if (checkElement(result, arr[i])) {
                result = arr[i];
            }
        }
        return result;
    }

    protected abstract boolean checkElement(int oldElement, int newElement);
}
