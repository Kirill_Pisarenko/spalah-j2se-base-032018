package com.spalah.nestedclasses.example1;

class A {

    private String data;

    public A(String data) {
        this.data = data;
    }

    class B {
        public String getData() {
            return data;
        }

        public void setData(String data) {
            A.this.data = data;
        }
    }

    IC getIC1() {
        return new IC() {
            @Override
            public void todo() {
                data = "Hello ANONIM";
            }
        };
    }

    IC getLocalIC() {
        class LocalIC implements IC {
            @Override
            public void todo() {
                data = "Hello Local";
            }
        };

        return new LocalIC();
    }


    @Override
    public String toString() {
        return "A{" +
                "data='" + data + '\'' +
                '}';
    }
}

interface IC {
    void todo();
}

public class App {
    public static void main(String[] args) {
        A a = new A("hello wrapper");
        A.B b = a.new B();

        System.out.println(a);
        System.out.println(b.getData());

        b.setData("Hello INNER CLASS!!!");
        System.out.println(a);
        System.out.println(b.getData());

        IC c = new IC() {
            @Override
            public void todo() {
                System.out.println("Hello IC");
            }
        };
        c.todo();

        a.getIC1().todo();
        System.out.println(a);
        a.getLocalIC().todo();

        System.out.println(a);
    }
}
