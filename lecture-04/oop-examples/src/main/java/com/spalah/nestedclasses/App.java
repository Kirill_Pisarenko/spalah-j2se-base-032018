package com.spalah.nestedclasses;

public class App {
    public static void main(String[] args) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Todo!");
            }
        });

        thread.start();
    }
}

//class Good implements Runnable {
//
//    @Override
//    public void run() {
//        System.out.println("Make Good");
//    }
//}