package com.spalah.collections.example3;


interface IA {
    int todo(int doSomethingGood);
}

class A implements IA {
    @Override
    public int todo(int ololo) {
        return ololo;
    }
}
public class App1 {
    public static void main(String[] args) {
        IA ia = new A();
        int blablablsa = 1000;
        System.out.println(ia.todo(blablablsa));
    }
}
