package com.spalah.collections.map;

import java.util.HashMap;
import java.util.Map;

class Person {
    private String name;
    private int age;
    private boolean sex;
    private boolean single;
    private Double salary;
    private int health;

    public Person() {
    }

    public Person(String name, int age, boolean sex, boolean single, Double salary, int health) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.single = single;
        this.salary = salary;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public boolean isSingle() {
        return single;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (sex != person.sex) return false;
        if (single != person.single) return false;
        if (health != person.health) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        return salary != null ? salary.equals(person.salary) : person.salary == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (sex ? 1 : 0);
        result = 31 * result + (single ? 1 : 0);
        result = 31 * result + (salary != null ? salary.hashCode() : 0);
        result = 31 * result + health;
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", single=" + single +
                ", salary=" + salary +
                ", health=" + health +
                '}';
    }
}

public class App {
    public static void main(String[] args) {
        Person sashaPerson = new Person("Sasha", 32, true, true, 5000., 100);
        Person bogdanPerson = new Person("Bogdan", 23, true, true, 5000., 100);

//        System.out.println((sashaPerson.hashCode() == bogdanPerson.hashCode()) && sashaPerson.equals(bogdanPerson));
//        System.out.println(sashaPerson.equals(bogdanPerson));

        Map<Person, String> map = new HashMap<>();
        map.put(sashaPerson, "Sasha");
        map.put(bogdanPerson, "Bogdan");

        System.out.println(map.size());

        System.out.println(map.values());
        System.out.println(map.keySet());

        for (Map.Entry<Person, String> entry : map.entrySet()) {
            Person key = entry.getKey();
            String value = entry.getValue();

            System.out.printf("key=%s; value=%s;%n", key, value);
        }

    }
}
