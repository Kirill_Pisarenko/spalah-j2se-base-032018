package com.spalah.collections.example2;

import java.util.Arrays;
import java.util.Iterator;

public class MyArrayList implements MyList {

    private static final int INIT_SIZE = 5;
    private int size;
    private Integer[] arr;

    public MyArrayList() {
        initArray();
    }

    @Override
    public void add(Integer value) {
        if (size >= arr.length) {
            increaseArraySize();
        }
        arr[size] = value;
        size++;
    }

    @Override
    public void clear() {
        initArray();
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Integer x) {
        return false;
    }

    @Override
    public Integer[] toArray() {
        return new Integer[0];
    }

    private void increaseArraySize() {
        arr = Arrays.copyOf(arr, arr.length * 2);
    }

    private void initArray() {
        arr = new Integer[INIT_SIZE];
    }

    @Override
    public void add(int index, Integer x) {

    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public int indexOf(Integer x) {
        return 0;
    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public void trimToSize() {

    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }
}
