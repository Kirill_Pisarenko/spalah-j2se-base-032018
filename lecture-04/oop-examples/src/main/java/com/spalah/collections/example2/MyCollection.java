package com.spalah.collections.example2;

public interface MyCollection extends Iterable<Integer> {

    void add(Integer value);

    void clear();

    int size();

    boolean isEmpty();

    boolean contains(Integer x);

    Integer[] toArray();
}
