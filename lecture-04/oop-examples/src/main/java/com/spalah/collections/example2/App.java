package com.spalah.collections.example2;

public class App {
    public static void main(String[] args) {
        MyCollection collection = new MyLinkedList();
        collection.add(1000);
        collection.add(2000);
        collection.add(3000);

        for (Integer value : collection) {
            System.out.println(value);
        }
    }
}
