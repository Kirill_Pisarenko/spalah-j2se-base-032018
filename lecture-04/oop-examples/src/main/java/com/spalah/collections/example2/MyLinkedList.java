package com.spalah.collections.example2;

import java.util.Iterator;

public class MyLinkedList implements MyCollection {

    private int size;
    private Node head;
    private Node tail;

    @Override
    public void add(Integer value) {
        Node node = new Node(value);
        if (isEmpty()) {
            head = node;
            tail = head;
        } else {
            tail.setNext(node);
            tail = node;
        }
        size++;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Integer value) {
        if (isEmpty()) {
            return false;
        }

        Node pointer = head;
        do {
            if (pointer.getValue().equals(value)) {
                return true;
            }
            pointer = pointer.getNext();
        } while (pointer == null);

        return false;
    }

    @Override
    public Integer[] toArray() {
        return new Integer[0];
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyCollectionIterator();
    }



    private class MyCollectionIterator implements Iterator<Integer>{

        private Node pointer;

        private MyCollectionIterator() {
            this.pointer = new Node(head);
        }

        @Override
        public boolean hasNext() {
            return pointer.getNext() != null;
        }

        @Override
        public Integer next() {
            pointer = pointer.getNext();
            return pointer.getValue();
        }
    }

    private static class Node {
        private Node next;

        private Integer value;

        public Node() {
        }

        public Node(Integer value) {
            this.value = value;
        }

        public Node(Node next) {
            this.next = next;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }
}
