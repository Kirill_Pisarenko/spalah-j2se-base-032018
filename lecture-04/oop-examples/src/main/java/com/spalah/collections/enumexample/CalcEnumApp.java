package com.spalah.collections.enumexample;

enum Operation {
    PLUS, MINUS, MULT, DIVIDE
}

public class CalcEnumApp {
    public static void main(String[] args) {

        int a = 100;
        int b = 200;
        Operation operation = Operation.PLUS;
        double res = calc(a, b, operation);
        printResult(a, b, operation, res);

        a = 100;
        b = 200;
        operation = Operation.MINUS;
        res = calc(a, b, operation);
        printResult(a, b, operation, res);


        a = 100;
        b = 200;
        operation = Operation.MULT;
        res = calc(a, b, operation);
        printResult(a, b, operation, res);


        a = 100;
        b = 200;
        operation = Operation.DIVIDE;
        res = calc(a, b, operation);
        printResult(a, b, operation, res);

    }

    public static void printResult(int leftOperand, int rightOperand, Operation operation, double result) {
        System.out.printf("a=%s; b=%s; operation=%s; result=%s%n", leftOperand, rightOperand, operation, result);
    }

    public static double calc(int leftOperand, int rightOperand, Operation operation) {
        switch (operation) {
            case PLUS:
                return leftOperand + rightOperand;
            case MINUS:
                return leftOperand - rightOperand;
            case MULT:
                return leftOperand * rightOperand;
            case DIVIDE:
                if (rightOperand == 0) {
                    throw new IllegalArgumentException("divide 0");
                }
                return  1. * leftOperand / rightOperand;

        }
        throw new IllegalArgumentException();
    }
}
