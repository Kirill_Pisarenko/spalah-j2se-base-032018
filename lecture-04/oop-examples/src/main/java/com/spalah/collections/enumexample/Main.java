package com.spalah.collections.enumexample;

import java.util.Arrays;

enum ErrorMessage {
    SERVICE_NOT_FOUND(100, "Service not found error"),
    INVALID_DATA(400, "Entered data invalid"),
    INTERNAT_ERROR(500, "Internal error");

    private final int code;
    private final String Message;

    ErrorMessage(int code, String message) {
        this.code = code;
        Message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return Message;
    }

    public static ErrorMessage fromCode(int code) {
        for (ErrorMessage errorMessage : ErrorMessage.values()) {
            if (errorMessage.code == code) {
                return errorMessage;
            }
        }
        return ErrorMessage.INTERNAT_ERROR;
    }
}

class Main {
    public static void main(String[] args) {
        ErrorMessage errorMessage = ErrorMessage.fromCode(100);
        System.out.println(errorMessage);
        System.out.println("code: " + errorMessage.getCode());
        System.out.println("message: "
                + errorMessage.getMessage());
        System.out.println("is INVALID_DATA: "
                + (errorMessage == ErrorMessage.INVALID_DATA));

        switch (errorMessage) {
            case SERVICE_NOT_FOUND:
                System.out.println("case: SERVICE_NOT_FOUND");
                break;
            case INVALID_DATA:
                System.out.println("case: INVALID_DATA");
                break;
            case INTERNAT_ERROR:
                System.out.println("case: INTERNAT_ERROR");
                break;
        }
    }
}

