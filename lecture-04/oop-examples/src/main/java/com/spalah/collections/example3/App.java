package com.spalah.collections.example3;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 0, 0, 0};
        System.out.println(Arrays.toString(arr));
        int index = 2;
        int length = 3;
        int value = 100;
        System.arraycopy(arr, index, arr, index + 1, length - index);
        arr[index] = value;
        System.out.println(Arrays.toString(arr));
    }
}
