package com.spalah.collections.example2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class AppIterator {
    public static void main(String[] args) {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1000);
        collection.add(2000);
        collection.add(3000);

        for (Integer value : collection) {
            System.out.println(value);
        }

//        Iterator<Integer> iterator = collection.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }
//
//        for (Iterator<Integer> iterator = collection.iterator(); iterator.hasNext(); ) {
//            System.out.println(iterator.next());
//        }
    }
}
